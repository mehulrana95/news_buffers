import { createGlobalStyle } from 'styled-components';

import oldstandardtt_bold from './OldStandardTT-Bold.ttf';
import oldstandardtt_italic from './OldStandardTT-Italic.ttf';
import oldstandardtt_regular from './OldStandardTT-Regular.ttf';
import lato_black from './Lato-Black.ttf';
import lato_light from './Lato-Light.ttf';

export default createGlobalStyle`
    @font-face {
        font-family: 'oldstandardtt_bold';
        src: local('oldstandardtt_bold'), local('oldstandardtt_bold'),
        url(${oldstandardtt_bold}) format('ttf'),
        url(${oldstandardtt_bold}) format('woff');
        font-weight: 300;
        font-style: normal;
    }
    @font-face {
        font-family: 'oldstandardtt_italic';
        src: local('oldstandardtt_italic'), local('oldstandardtt_italic'),
        url(${oldstandardtt_italic}) format('ttf');
        font-weight: 300;
        font-style: normal;
    }
    @font-face {
        font-family: 'oldstandardtt_regular';
        src: local('oldstandardtt_regular'), local('oldstandardtt_regular'),
        url(${oldstandardtt_regular}) format('ttf');
        font-weight: 300;
        font-style: normal;
    }
    @font-face {
        font-family: 'lato_black';
        src: local('lato_black'), local('lato_black'),
        url(${lato_black}) format('ttf')
        url(${lato_black}) format('woff');
        font-style: normal;
    }
    @font-face {
        font-family: 'lato_light';
        src: local('lato_light'), local('lato_light'),
        url(${lato_light}) format('ttf')
        url(${lato_light}) format('woff');
        font-style: normal;
    }
`;