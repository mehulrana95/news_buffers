import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.scss';
import Login from '../Login';


class Header extends React.Component {
	render() {
		return (
			<div>
				<Login />
				<h3 className="text-center title">News Buffers</h3>
			</div>
		)
	}
}


export default Header;
