import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Footer.scss';
import logo from '../../images/logo.png';


class Footer extends React.Component {
	render() {
		return (
			<div className="footer_parent_div">
				<img src={logo} alt="News Buffers Logo" className="footerlogo" />
				<div className="socials">
					<div className="social_container">
						<div className="facebook"><a href=""><i className="fab fa-facebook-f"></i></a></div>
						<div className="twitter"><a href=""><i className="fab fa-twitter"></i></a></div>
						<div className="instagram"><a href=""><i className="fab fa-instagram"></i></a></div>
						<div className="google"><a href=""><i className="fab fa-google-plus-g"></i></a></div>
					</div>
				</div>
				<p className="text-center copyright">&copy; Copyright 2020, All rights reserved. Designed with <i className="fas fa-heart"></i></p>
			</div>
		)
	}
}


export default Footer;
