import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Home.scss';
import $ from 'jquery';


class Home extends React.Component {
	constructor(props) {
		super(props);
		this.image = React.createRef();
		this.state = {
			error: null,
			isLoaded: true,
			articles: [],
		};
	}

	componentDidMount() {
		fetch("https://newsapi.org/v2/top-headlines?country=in&apiKey=a79549e725f243deaf7cf0eba53220d0")
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLoaded: true,
						articles: result.articles
					});
				},
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			)
	}
	render() {
		const { error, isLoaded, articles } = this.state;
		if (error) {
			return <div>Error: {error.message} </div>
		}
		else if (!isLoaded) {
			return <div>Loading...</div>
		}
		else {
			console.log(articles);
			return (
				<div>
					<section className="lp">
						{articles.map(item => (
						<a href={item.url}>
							<div className="row outer">
								<div className="col-md-2 imageArticleDiv" ref={this.image}>
									<div className="articleImage" ref={this.articleImage}>
										<img src={item.urlToImage} />
									</div>
								</div>
								<div className="col-md-1"></div>
								<div className="col-md-9">
									<p className="articleTitle">{ item.title }</p>
									<p className="articleDesc">{ item.description }</p>
									<div className="row">
										<div className="col-md-4">
											<p className="author">Author: <span className="highlight">{ item.author }</span></p>
										</div>
										<div className="col-md-4">
											<p className="source">Source: <span className="highlight">{ item.source.name }</span></p>
										</div>
										<div className="col-md-4">
											<p className="date">Date: <span className="highlight">{ item.publishedAt }</span></p>
										</div>
									</div>
								</div>
							</div>
						</a>
						))}
					</section>
				</div>
			);
		}
	}
}


export default Home;
