import React from 'react';
import GlobalFonts from './fonts/fonts';
import logo from './logo.png';
import './App.css';
import $ from 'jquery';

// Import components

import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div className="container">
      <GlobalFonts />
      <Header />
      <Home />
      <Footer />   
    </div>
  );
}

export default App;
